const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const tennisEventSchema = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  country: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  location: String,
  city: String,
  surface: String,
  type: String,
  season: {
    type: String,
    required: true,
  },
  id: {
    type: String,
    required: true,
  },
  fixture: String,
  standings: String,
  history: {
    item: [String],
  },
  historyP2P: {
    item: [String],
  },
  historyStats: {
    item: [String],
  },
});

const TES = mongoose.model("tes", tennisEventSchema);

module.exports = { TES };
