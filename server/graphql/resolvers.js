const { TES } = require("../modals/User");

const queryResolvers = {
  getTennisEvents: async (_, { startIdx, endIdx }) => {
    let res = await TES.find()
      .skip(startIdx)
      .limit(endIdx - startIdx);

    return res;
  },

  getCSV: async (_, { id, location, season, startIdx, endIdx }) => {
    // Build the query dynamically based on provided fields
    let query = {};
    if (id) query._id = id;
    if (location) query.location = location;
    if (season) query.season = season;

    // Find documents that match the constructed query
    let res = await TES.find(query, {
      _id: 1,
      location: 1,
      season: 1,
      country: 1,
      name: 1,
    })
      .skip(startIdx)
      .limit(endIdx - startIdx);

    return res;
  },

  getCount: async (_, { id, location, season, startIdx, endIdx }) => {
    // Build the query dynamically based on provided fields
    let query = {};
    if (id) query._id = id;
    if (location) query.location = location;
    if (season) query.season = season;

    // Find documents that match the constructed query
    let res = await TES.countDocuments(query);
    return res;
  },
};

module.exports = queryResolvers;
