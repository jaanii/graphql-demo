const typeDefs = `
type TennisEvent {
  _id: ID!
  country : String!
  name : String!
  location : String!
  fixture: String!
  standings: String
  season : String!
  history : History!
  historyP2P: HistoryP2P!
  historyStats: HistoryStats!
} 

type History {
  item: [String!]!
}

type HistoryP2P {
  item: [String!]!
}

type HistoryStats {
  item: [String!]!
}

type Query {
  getTennisEvents (startIdx : Int!, endIdx : Int!) : [TennisEvent],
  getCSV(id: ID, location : String, season : String, startIdx : Int!, endIdx : Int!) : [TennisEvent]
  getCount(id: ID, location : String, season : String) : Int!
}
`;

module.exports = typeDefs;
