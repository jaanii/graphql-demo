const queries = `
  getTennisEvents (startIdx : Int!, endIdx : Int!) : [TennisEvent],
  getCSV(id: ID, location : String, season : String, startIdx : Int!, endIdx : Int!) : [TennisEvent]
  getCount(id: ID, location : String, season : String) : Int!
`;

module.exports = queries;
