// express
const express = require("express");

// apollo
const { ApolloServer } = require("@apollo/server");

// middleware as we would accept form input
const { expressMiddleware } = require("@apollo/server/express4");
const bodyParser = require("body-parser");

// cors
const cors = require("cors");

// db connection
const connectToMongoDB = require("./db");

// graphql
const typeDefs = require("./graphql/typedefs");
const queries = require("./graphql/queries");
const queryResolvers = require("./graphql/resolvers");

// port & express
const PORT = 8080;
const app = express();

// using cors
app.use(cors());

// creating apolloserver
async function startServer() {
  //
  const server = new ApolloServer({
    typeDefs: `

    ${typeDefs}
    
    type query  {
      ${queries}
    }
    `,

    // resolvers
    resolvers: {
      // query resolvers
      Query: queryResolvers,
    },
  });

  // middleware implementation
  app.use(bodyParser.json());

  // starting apollo server
  await server.start();

  // expoxing endpoint
  app.use("/graphql", expressMiddleware(server));

  // express server
  app.listen(PORT, () => {
    console.log(`Server running on: http://localhost:${PORT}`);
  });
}

// calling func to start server
startServer();

// db connection
connectToMongoDB();
