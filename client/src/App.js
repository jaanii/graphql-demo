import { gql, useQuery } from "@apollo/client";
import "./App.css";
import { useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Navbar from "./pages/Navbar";
import Tables from "./pages/Tables";
import Test from "./pages/test";

function App() {
  const [indexes, setIndexes] = useState({
    startIdx: 0,
    endIdx: 5,
  });

  const GET_DATA = gql`
    query GetTennisEvents($startIdx: Int!, $endIdx: Int!) {
      getTennisEvents(startIdx: $startIdx, endIdx: $endIdx) {
        _id
        name
        location
        country
        season
      }
    }
  `;

  const GET_DATA_SIZE = gql`
    query GetCount($id: ID, $location: String, $season: String) {
      getCount(id: $id, location: $location, season: $season)
    }
  `;

  const { refetch: getCount } = useQuery(GET_DATA_SIZE);

  const { data, refetch: refreshData } = useQuery(GET_DATA);

  return (
    <Router>
      <Navbar />
      <Routes>
        <Route
          path="/tables"
          element={
            <Tables
              data={data}
              refreshData={refreshData}
              indexes={indexes}
              setIndexes={setIndexes}
              getCount={getCount}
            />
          }
        />
        <Route path="" element={<Test />} />
      </Routes>
    </Router>
  );
}

export default App;
