import { Box, Button, Stack, TablePagination, TextField } from "@mui/material";
import React, { useEffect, useState } from "react";

import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { gql, useQuery } from "@apollo/client";

import imgs from "./pebble-beach.jpeg";

const GET_DATA_BY_CSV = gql`
  query GetCSV(
    $id: ID
    $location: String
    $season: String
    $startIdx: Int!
    $endIdx: Int!
  ) {
    getCSV(
      id: $id
      location: $location
      season: $season
      startIdx: $startIdx
      endIdx: $endIdx
    ) {
      _id
      country
      location
      season
      name
    }

    getCount(id: $id, location: $location, season: $season)
  }
`;

const Tables = ({ refreshData, indexes, setIndexes, getCount }) => {
  const [info, setInfo] = useState();
  const [searchID, setSearchID] = useState("");
  const [searchLocation, setSearchLocation] = useState("");
  const [searchSeason, setSearchSeason] = useState("");
  const [count, setCount] = useState(6);
  const { refetch: getCSV } = useQuery(GET_DATA_BY_CSV);

  function createData(_id, name, country, location, season) {
    return { _id, name, country, location, season };
  }

  const rows = info?.map((item) => {
    const { _id, name, country, location, season } = item;
    let row = createData(_id, name, country, location, season);
    return row;
  });

  const handleSearchChange = (e) => {
    let val = e.target.value;
    let searchFields = val.split(", ");
    for (var a in searchFields) {
      var variable = searchFields[a];
      if (variable[0] === "6") {
        setSearchID(variable.trim());
      } else if (variable[0] === "2") {
        setSearchSeason(variable.trim());
      } else {
        setSearchLocation(variable.trim());
      }
    }
  };

  const serarchData = async () => {
    try {
      let data = await getCSV({
        id: searchID !== "" ? searchID : "",
        location: searchLocation !== "" ? searchLocation : "",
        season: searchSeason !== "" ? searchSeason : "",
        startIdx: indexes.startIdx,
        endIdx: indexes.endIdx,
      });

      let dataSize = await getCount({
        id: searchID !== "" ? searchID : "",
        location: searchLocation !== "" ? searchLocation : "",
        season: searchSeason !== "" ? searchSeason : "",
      });
      setCount(dataSize.data.getCount);
      setInfo(data.data.getCSV);
      setSearchID("");
      setSearchLocation("");
      setSearchSeason("");
    } catch (e) {
      console.log(e);
    }
  };

  const getData = async () => {
    try {
      let data = await refreshData({
        startIdx: indexes.startIdx,
        endIdx: indexes.endIdx,
      });
      setInfo(data?.data?.getTennisEvents);
      let len = await getCount();
      setCount(len.data.getCount);
    } catch (e) {
      console.log(e);
    }
  };

  const [page, setPage] = useState(0);

  const handleChangePage = async (event, newPage) => {
    setPage(newPage);
    try {
      if (newPage > page) {
        serarchData();
        setIndexes({
          startIdx: indexes.endIdx,
          endIdx: indexes.endIdx + 5,
        });
      } else {
        serarchData();
        setIndexes({
          startIdx: indexes.startIdx - 5 > 0 ? indexes.startIdx - 5 : 0,
          endIdx: indexes.startIdx,
        });
      }
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <Box
      sx={{
        position: "fixed",
        top: "0",
        left: "0",
      }}
    >
      <img src={imgs} alt="" />
      <Box
        sx={{
          display: "flex",
          width: "100%",
          justifyContent: "space-around",
          alignItems: "center",
          margin: "20px auto",
        }}
      >
        <TextField
          placeholder="Enter Year to filter"
          sx={{
            width: "50%",
            margin: "0 20px",
          }}
          onChange={handleSearchChange}
          autoComplete="off"
        />
        <Button
          sx={{
            width: "20%",
            padding: "15px 0",
            margin: "0 20p",
          }}
          variant="contained"
          onClick={serarchData}
        >
          Search
        </Button>
      </Box>

      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell align="right">Name</TableCell>
              <TableCell align="right">Country</TableCell>
              <TableCell align="right">Location</TableCell>
              <TableCell align="right">Season</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows?.map((row) => (
              <TableRow
                key={row._id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {row._id}
                </TableCell>
                <TableCell align="right">{row.name}</TableCell>
                <TableCell align="right">{row.country}</TableCell>
                <TableCell align="right">{row.location}</TableCell>
                <TableCell align="right">{row.season}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Stack>
        <TablePagination
          sx={{
            width: "100%",
            margin: "auto",
            backgroundColor: "#171E2E",
            color: "white",
            alignItems: "center",
          }}
          component="div"
          count={count}
          rowsPerPage={5}
          page={page}
          onPageChange={handleChangePage}
        />
      </Stack>
    </Box>
  );
};

export default Tables;
